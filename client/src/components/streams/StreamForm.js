import React, {Component} from 'react';
import {Field, reduxForm} from "redux-form";

class StreamForm extends Component {
    renderInput = ({input, label, meta}) => {
        const className = `field ${meta.error && meta.touched ? 'error' : ''}`;
        return (
            <div className={className}>
                <label>{label}</label>
                <input {...input} autoComplete={'off'}/>
                {this.renderError(meta)}
            </div>

        );
    };

    renderError({error, touched}) {
        return (touched && <div className={'ui error message'}>{error}</div>)
    };

    onSubmit = (formValues) => {
        this.props.onSubmit(formValues)
    };

    render() {
        return (
            <form onSubmit={this.props.handleSubmit(this.onSubmit)} className={'ui form error'}>
                <Field name={'title'} component={this.renderInput} label={'Enter Title'}/>
                <Field name={'description'} component={this.renderInput} label={'Enter Description'}/>
                <button className={'ui primary button'}>Submit</button>
            </form>
        );
    }
}

const validate = (formValues) => {
    let errors = {};
    if (!formValues.title) {
        errors.title = 'please insert title'
    }
    if (!formValues.description) {
        errors.description = 'please insert description'
    }
    return errors;
};

export default reduxForm({form: 'streamForm', validate})(StreamForm)

