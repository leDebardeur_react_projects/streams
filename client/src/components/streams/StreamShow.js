import React, {Component} from 'react';
import {fetchStream} from "../../actions";
import {connect} from "react-redux";
import flv from 'flv.js'

class StreamShow extends Component {
    constructor(props) {
        super(props);
        this.videoRef = React.createRef()
    }

    componentDidMount() {
        this.props.fetchStream(this.props.match.params.id);
        this.buidPlayer()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.buidPlayer()
    }

    buidPlayer() {
        if (this.player || !this.props.stream) {
            return;
        }
        this.player = flv.createPlayer({
            type: 'flv',
            url: `http://localhost:8000/live/${this.props.match.params.id}.flv`
        });
        this.player.attachMediaElement(this.videoRef.current);
        this.player.load();
    }

    renderContent() {
        if (!this.props.stream) {
            return (
                <div className="ui active inverted dimmer">
                    <div className="ui text loader">Loading</div>
                </div>
            )
        } else {
            const {title, description} = this.props.stream;
            return (
                <div>
                    <video ref={this.videoRef} style={{width: '100%'}} controls></video>
                    <h1>{title}</h1>
                    <h5>{description}</h5>

                </div>
            )
        }
    }
    componentWillUnmount() {
        this.player.destroy();
    }

    render() {
        return (
            <div>
                {this.renderContent()}
            </div>
        );
    }
}

let mapStateToProps = (state, ownProps) => {
    return ({
        stream: state.streams[ownProps.match.params.id]
    })
};
export default connect(mapStateToProps, {fetchStream})(StreamShow);